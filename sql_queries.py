
accounts= """SELECT
					DATE_TRUNC('month', created_at) as month,
					COUNT(1) as new_amount
					FROM accounts
					WHERE created_at > '{}'
					AND created_at < '{}'
					AND status IN ('active')
					GROUP BY month
					ORDER BY month"""

subscriptions = """SELECT
					DATE_TRUNC('month', created_at) as month,
					COUNT(1) as new_amount
					FROM subscriptions
					WHERE created_at > '{}'
					AND created_at < '{}'
					AND status IN ('active')
					GROUP BY month
					ORDER BY month"""
users = """SELECT
					DATE_TRUNC('month', created_at) as month,
					COUNT(1) as new_amount
					FROM users
					WHERE created_at > '{}'
					AND created_at < '{}'
					AND status IN ('active')
					GROUP BY month
					ORDER BY month"""

charges_by = """SELECT
					DATE_TRUNC('month', close_date) as month,
					SUM(amount) as total
					FROM charges as c
					JOIN subscriptions as s on s.id = c.subscription_id
					JOIN accounts as a on s.account_id = a.id
					WHERE close_date > '{}'
					AND close_date < '{}'
					AND c.status = 'closed'
					AND a.vendor_id = 1000002
					AND a.account_class_id != 235
					GROUP BY month
					ORDER BY month"""

charges_ru = """SELECT
					DATE_TRUNC('month', close_date) as month,
					SUM(amount) as total
					FROM charges as c
					JOIN subscriptions as s on s.id = c.subscription_id
					JOIN accounts as a on s.account_id = a.id
					WHERE close_date > '{}'
					AND close_date < '{}'
					AND c.status = 'closed'
					AND a.vendor_id = 1001024
					AND a.account_class_id != 2000017
					GROUP BY month
					ORDER BY month"""

charges = """SELECT
					DATE_TRUNC('month', close_date) as month,
					SUM(amount) as total
					FROM charges as c
					JOIN subscriptions as s on s.id = c.subscription_id
					JOIN accounts as a on s.account_id = a.id
					WHERE close_date > '{}'
					AND close_date < '{}'
					AND c.status = 'closed'
					GROUP BY month
					ORDER BY month"""

vendor_charges = """SELECT
					DATE_TRUNC('month', created_at ) as month,
					SUM(amount) as total
					FROM vendor_charges as c
					WHERE created_at > '{}'
					AND created_at < '{}'
					AND c.parent_vendor_id IN (1)
					GROUP BY month
					ORDER BY month"""

accounts_to_date = "Select count(1) as amount from accounts where created_at < '{}' AND status IN ('active')"

users_to_date = "Select count(1) as amount from users where created_at < '{}' AND status IN ('active')"

subscriptions_to_date = "Select count(1) as amount from subscriptions where created_at < '{}' AND status IN ('active')"


