from db_credentials import *
from sql_queries import *
import pandas as pd
import psycopg2
import datetime as dt
import os


#HELPERS
def get_timestamp():
	return dt.datetime.now().strftime(("%Y_%m_%d_%H_%M_%S"))


def total_users(date_to, conn):
	cur = conn.cursor()
	cur.execute(users_to_date.format(date_to))
	return cur.fetchone()[0]

def total_accounts(date_to, conn):
	cur = conn.cursor()
	cur.execute(accounts_to_date.format(date_to))
	return cur.fetchone()[0]

def total_subs(date_to, conn):
	cur = conn.cursor()
	cur.execute(subscriptions_to_date.format(date_to))
	return cur.fetchone()[0]

def generate_report_for_ac(date_from, date_to):
	print("Fetching data from AC DB... ")
	connection = psycopg2.connect(**ac_pg_slave_config)
	df_accounts = pd.read_sql_query(accounts.format(date_from, date_to), con = connection)
	df_subscriptions = pd.read_sql_query(subscriptions.format(date_from, date_to), con = connection)
	df_users = pd.read_sql_query(users.format(date_from, date_to), con = connection)
	df_charges_by = pd.read_sql_query(charges_by.format(date_from, date_to), con = connection)
	df_charges_ru = pd.read_sql_query(charges_ru.format(date_from, date_to), con = connection)
	df_accounts = df_accounts.sort_values(by="month")
	df_users = df_users.sort_values(by="month")
	df_charges_by = df_charges_by.sort_values(by="month")
	df_charges_ru = df_charges_ru.sort_values(by="month")
	df_accounts['total_accounts_to_date'] = df_accounts.apply(lambda x: total_accounts(x['month'], connection), axis = 1)
	df_users['total_users_to_date'] = df_users.apply(lambda row: total_users(row['month'], connection), axis = 1)
	df_subscriptions['total_subscriptions_to_date'] = df_subscriptions.apply(lambda row: total_subs(row['month'], connection), axis = 1)
	connection.close()
	print("Done.")
	print("Writing data to file..")
	report_file = os.path.join("./generated_reports", f"ac_statistics_{get_timestamp()}.xlsx")

	with pd.ExcelWriter(report_file, engine='openpyxl') as writer:
		df_users.to_excel(writer, 'users', index=False)
		df_accounts.to_excel(writer, 'accounts', index=False)
		df_charges_by.to_excel(writer, 'charges_by', index=False)
		df_charges_ru.to_excel(writer, 'charges_ru', index=False)
		df_subscriptions.to_excel(writer, 'subscriptions', index=False)
	print("Done.")



def generate_report_for_sl(date_from, date_to):
	print("Fetching data from SL DB... ")
	connection = psycopg2.connect(**sl_pg_slave_config)
	df_accounts = pd.read_sql_query(accounts.format(date_from, date_to), con = connection)
	df_subscriptions = pd.read_sql_query(subscriptions.format(date_from, date_to), con = connection)
	df_users = pd.read_sql_query(users.format(date_from, date_to), con = connection)
	df_vendor_charges = pd.read_sql_query(vendor_charges.format(date_from, date_to), con = connection)
	df_accounts = df_accounts.sort_values(by="month")
	df_users = df_users.sort_values(by="month")
	df_vendor_charges = df_vendor_charges.sort_values(by="month")
	df_accounts['total_accounts_to_date'] = df_accounts.apply(lambda x: total_accounts(x['month'], connection), axis = 1)
	df_users['total_users_to_date'] = df_users.apply(lambda row: total_users(row['month'], connection), axis = 1)
	df_subscriptions['total_subscriptions_to_date'] = df_subscriptions.apply(lambda row: total_subs(row['month'], connection), axis = 1)
	connection.close()
	print("Done.")
	print("Writing data to file..")
	report_file = os.path.join("./generated_reports", f"sl_statistics_{get_timestamp()}.xlsx")

	with pd.ExcelWriter(report_file, engine='openpyxl') as writer:
		df_users.to_excel(writer, 'users', index=False)
		df_accounts.to_excel(writer, 'accounts', index=False)
		df_vendor_charges.to_excel(writer, 'vendor_charges', index=False)
		df_subscriptions.to_excel(writer, 'subscriptions', index=False)
	print("Done.")


def generate_report_for_huz(date_from, date_to):
	print("Fetching data from HUZ DB... ")
	connection = psycopg2.connect(**huz_pg_slave_config)
	df_accounts = pd.read_sql_query(accounts.format(date_from, date_to), con = connection)
	df_subscriptions = pd.read_sql_query(subscriptions.format(date_from, date_to), con = connection)
	df_users = pd.read_sql_query(users.format(date_from, date_to), con = connection)
	df_charges = pd.read_sql_query(charges.format(date_from, date_to), con = connection)
	df_accounts = df_accounts.sort_values(by="month")
	df_users = df_users.sort_values(by="month")
	df_charges = df_charges.sort_values(by="month")
	df_accounts['total_accounts_to_date'] = df_accounts.apply(lambda x: total_accounts(x['month'], connection), axis = 1)
	df_users['total_users_to_date'] = df_users.apply(lambda row: total_users(row['month'], connection), axis = 1)
	df_subscriptions['total_subscriptions_to_date'] = df_subscriptions.apply(lambda row: total_subs(row['month'], connection), axis = 1)
	connection.close()
	print("Done.")
	print("Writing data to file..")
	report_file = os.path.join("./generated_reports", f"huz_statistics_{get_timestamp()}.xlsx")

	with pd.ExcelWriter(report_file, engine='openpyxl') as writer:
		df_users.to_excel(writer, 'users', index=False)
		df_accounts.to_excel(writer, 'accounts', index=False)
		df_charges.to_excel(writer, 'charges', index=False)
		df_subscriptions.to_excel(writer, 'subscriptions', index=False)
	print("Done.")


