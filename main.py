import click
import reporter

@click.command()
@click.argument('from_date')
@click.argument('to_date')

def main(from_date, to_date):
	reporter.generate_report_for_ac(from_date, to_date)
	reporter.generate_report_for_sl(from_date, to_date)
	reporter.generate_report_for_huz(from_date, to_date)

if __name__ == '__main__':
	main()